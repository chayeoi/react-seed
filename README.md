# Seed

## Repository Link

[Repository Link](https://bitbucket.org/chayeoi/react-seed/src/master/)

```bash
git clone https://chayeoi@bitbucket.org/chayeoi/react-seed.git
```

## Scaffolding

페이스북에서 공식적으로 지원하는 스캐폴딩 도구인 CRA(create-react-app)를 이용해 boilerplate를 생성한 후 `eject` 명령을 실행해 커스터마이징 환경을 구성했습니다.

## Coding Style

Airbnb 스타일 가이드를 대부분 준수하고 몇 가지 예외 규칙을 적용했습니다.

### ESLint

```json
# .eslintrc.json
{
  "parser": "babel-eslint",
  "settings": { "import/resolver": { "node": { "paths": ["src"] } } },
  "env": {
    "browser": true,
    "jest": true
  },
  "extends": "airbnb",
  "rules": {
    "semi": ["error", "never"],
    "react/jsx-filename-extension": ["error", { "extensions": [".js"] }],
    "react/forbid-prop-types": ["off"],
    "import/no-extraneous-dependencies": "off",
    "import/prefer-default-export": "off",
    "no-underscore-dangle": ["error", { "allow": ["__REDUX_DEVTOOLS_EXTENSION_COMPOSE__"] }]
  }
}
```

```json
# .eslintignore
config/
scripts/
src/registerServiceWorker.js
```

1. Airbnb 스타일가이드 적용 (Line 8)
2. `babel-eslint` 적용: 아직은 실험적인 기능인 `Class Fields`와 같은 최신 문법에 대해 ESLint가 에러를 내뱉는 일 없이 사용하기 위함 (Line 2)
3. `NODE_PATH=src` 적용: 절대 경로로 import할 ESLint가 때 루트 디렉토리를 `src`로 인식하기 위함 (Line 3)
4. 브라우저 및 테스트 실행 환경 적용: 브라우저 실행 환경 및 테스트 실행 환경에서 사용되는 전역 프로퍼티 및 함수를 경고없이 사용하기 위함(ex: `window`, `describe`, `it`...) (Line 4 ~ 7)
5. 세미콜론 사용하지 않음 (Line 10)
6. 파일 확장자 '.js'만 허용 (Line 11)
7. Type Checking을 위한 PropTypes 사용 (Line 12)
8. No Extraneous Dependencies 적용 해제: devDependency로 추가된 Storybook의 `storiesOf` 함수를 경고없이 사용하기 위함 (Line 13)
9. Prefer Default Export 적용 해제: 하나의 모듈만 export하는 상황에서 default export 사용 강요로 인한 경고를 없애기 위함 (Line 14)
10. Redux Devtools 설정 변수에 언더스코어(_) 문자로 시작하는 변수명 허용 (Line 15)
11. `config/`, `scripts/`, `src/registerServiceWorker.js` 등 CRA의 기본 설정 파일들은 ESLint 규칙 적용 대상에서 제외 (.eslintingore)

### .editorconfig

```text
# .editorconfig
root = true

[*]
indent_style = space
indent_size = 2
end_of_line = lf
charset = utf-8
trim_trailing_whitespace = true
insert_final_newline = true
max_line_length = 80

[*.md]
trim_trailing_whitespace = false
```

## Testing

Jest + Enzyme 조합을 이용한 테스팅 환경을 구성했습니다. testing 파일의 이름은 `/.test.js$/`로 작성하고 테스트할 컴포넌트 파일과 같은 디렉토리에 위치시키면 됩니다.

## Storybook

### Storybook이 필요한 이유

[스토리북](https://storybook.js.org/)을 사용하기로 한 이유는 다음과 같습니다.

1. state가 특정한 값을 취했을 때에만 화면에 그려지는 UI 컴포넌트들이 있습니다. 이러한 컴포넌트를 개발하기 위해 코드를 수정 후 저장하는 순간, Webpack의 Hot Reloading로 인해 state는 초기화되고 이에 따라 해당 UI 컴포넌트도 초기화되버립니다. 코드를 수정할 때마다 개발 중인 UI 컴포넌트를 시각적으로 확인하기 위해 매번 state를 조작해야 한다면 매우 번거롭게 느껴질 것입니다. 그러나 Storybook을 사용하면 프로젝트와 독립된 환경에서 실행함으로써 이러한 문제에 얽매이지 않고 각 state에 따른 UI 컴포넌트의 스냅샷을 확인할 수 있습니다.
2. A라는 컴포넌트가 B라는 컴포넌트에 종속적일 수 있습니다. 이때 A 컴포넌트의 UI를 확인하기 위해서는 반드시 B 컴포넌트를 먼저 화면에 렌더링해야 합니다. 만약 이 종속 관계가 깊어진다면, 계층 관계의 제일 아래에 놓인 컴포넌트를 시각적으로 확인하기 위해서는 계층 관계에 놓여있는 각 컴포넌트 간에 종속적인 관계를 일일이 명시하고 제일 상위 계층에 놓인 컴포넌트를 반드시 화면에 렌더링해야 합니다. Storybook을 사용하면 컴포넌트 간에 종속성에서 벗어남으로써 이 문제를 해결할 수 있습니다.
3. Storybook을 따로 Static App 형태로 디플로이할 수 있습니다. 이를 통해 개발자와 디자이너를 포함한 팀원 모두가 UI 컴포넌트를 공유하게 됨으로써 협업하는 데 도움이 될 수 있을 것 같습니다.

### Storybook 설정

stories 파일(`/.stories.js$/`)은 코드 스플리팅이 필요없는 개발(Development) 모드에서만 사용되기 때문에, `require.context()` API를 통하여 불러오도록 설정했습니다.

```javascript
// .storybook/config.js
import { configure } from '@storybook/react'

const req = require.context('components', true, /.stories.js$/)

function loadStories() {
  req.keys().forEach(filename => req(filename))
}

configure(loadStories, module)
```

이렇게 함으로써 각 stories 파일들을 따로 import해야하는 과정이 없어지고 컴포넌트 파일과 같은 디렉토리에 위치시킬 수 있게 됩니다.

## CSS Styling

React 생태계에서 요즘 가장 인기있는 스타일링 방식 중 하나인 Styled Components를 사용할 수 있도록 구성했습니다. Styled Components를 선택한 이유는 다음과 같습니다.

1. Styled Components를 사용함으로써 `.js` 파일에서 `.css` 또는 `.scss` 파일을 import하지 않아도 되고, 불러온 클래스를 DOM 엘리먼트 또는 React 엘리먼트에 일일이 맵핑할 필요가 없어집니다.
2. 관심사의 분리(SepratIon of Concerns)라는 관점에서 바라봤을 때, 전통적인 웹 개발 방식에서는 구조와 스타일링을 파일 단위로 분리(.html과 .css)하는 것이 훌륭한 방법이었지만, 최근의 컴포넌트 기반 개발(CBD) 트렌드에서는 Styled Components와 같은 CSS-in-JS가 그 의미에 더 부합하는 방식인 것 같습니다. 하나의 컴포넌트에 대한 관심사를 모두 한 파일 내에 모아둠으로써 여러 개의 파일을 넘나들 필요가 없어지기 때문에, 뛰어난 개발 경험을 제공받을 수 있는 것 같습니다.
3. React 생태계에서 최근 가장 주목받는 동시에 유망한 스타일링 방식입니다.

## Environment

### Easy Import를 위한 NODE_PATH 설정

절대 경로로 파일을 import할 때 루트 디렉토리를 `src`로 인식하도록 .env 파일에 `NODE_PATH` 설정 (참고: [CRA 공식문서](https://github.com/facebook/create-react-app/blob/master/packages/react-scripts/template/README.md#advanced-configuration))

```plain
# .env
NODE_PATH=src
```
